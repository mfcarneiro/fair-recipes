import 'package:flutter/material.dart';

import 'recipe.dart';

class RecipeDetail extends StatefulWidget {
  final Recipe recipe;

  const RecipeDetail({Key? key, required this.recipe}) : super(key: key);

  @override
  State createState() => _RecipeDetailState();
}

class _RecipeDetailState extends State<RecipeDetail> {
  int _slideValue = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.recipe.label),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: _buildRecipeDetail(),
      ),
    );
  }

  Widget _buildRecipeDetail() {
    return SafeArea(
      maintainBottomViewPadding: true,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 300,
            width: double.infinity,
            child: Image(
              image: AssetImage(widget.recipe.imageUrl),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                widget.recipe.label,
                style: const TextStyle(fontSize: 18),
              ),
            ],
          ),
          const SizedBox(
            height: 24,
          ),
          const SizedBox(
            height: 24,
            child: Text(
              'Ingredients',
              style: TextStyle(
                fontSize: 11,
                fontWeight: FontWeight.w500,
                color: Colors.brown,
                decorationColor: Colors.brown,
              ),
            ),
          ),
          Expanded(
            child: ListView.builder(
              padding: const EdgeInsets.all(7.0),
              itemCount: widget.recipe.ingredients.length,
              itemBuilder: (BuildContext context, int index) {
                final ingredient = widget.recipe.ingredients[index];
                return Text(
                  '${ingredient.quantity * _slideValue} ${ingredient.measure} ${ingredient.name}',
                  style: const TextStyle(
                      fontSize: 16.0, fontWeight: FontWeight.w400),
                );
              },
            ),
          ),
          Slider(
            label: '${_slideValue * widget.recipe.servings} servings',
            min: 1,
            max: 10,
            divisions: 10,
            inactiveColor: Colors.grey,
            value: _slideValue.toDouble(),
            onChanged: (newValue) {
              setState(() {
                _slideValue = newValue.round();
              });
            },
          )
        ],
      ),
    );
  }
}
