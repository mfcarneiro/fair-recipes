import 'package:fair_recipes/ingretients.dart';

class Recipe {
  String label;
  String imageUrl;
  int servings;
  List<Ingredient> ingredients;

  Recipe({
    required this.label,
    required this.imageUrl,
    required this.servings,
    required this.ingredients,
  });

  static List<Recipe> samples = [
    Recipe(
      label: 'Spaghetti and Meatballs',
      imageUrl: 'assets/2126711929_ef763de2b3_w.jpg',
      servings: 4,
      ingredients: [
        Ingredient(
          1,
          'box',
          'Spaghetti',
        ),
        Ingredient(
          4,
          '',
          'Frozen Meatballs',
        ),
        Ingredient(
          0.5,
          'jar',
          'sauce',
        ),
      ],
    ),
    Recipe(
      label: 'Tomato Soup',
      imageUrl: 'assets/27729023535_a57606c1be.jpg',
      servings: 2,
      ingredients: [
        Ingredient(
          1,
          'can',
          'Tomato Soup',
        ),
      ],
    ),
    Recipe(
      label: 'Grilled Cheese',
      imageUrl: 'assets/3187380632_5056654a19_b.jpg',
      servings: 1,
      ingredients: [
        Ingredient(
          2,
          'slices',
          'Cheese',
        ),
        Ingredient(
          2,
          'slices',
          'Bread',
        ),
      ],
    ),
    Recipe(
      label: 'Chocolate Chip Cookies',
      imageUrl: 'assets/15992102771_b92f4cc00a_b.jpg',
      servings: 24,
      ingredients: [
        Ingredient(
          4,
          'cups',
          'flour',
        ),
        Ingredient(
          2,
          'cups',
          'sugar',
        ),
        Ingredient(
          0.5,
          'cups',
          'chocolate chips',
        ),
      ],
    ),
    Recipe(
      label: 'Taco Salad',
      imageUrl: 'assets/8533381643_a31a99e8a6_c.jpg',
      servings: 1,
      ingredients: [
        Ingredient(
          4,
          'oz',
          'nachos',
        ),
        Ingredient(
          3,
          'oz',
          'taco meat',
        ),
        Ingredient(
          0.5,
          'cup',
          'cheese',
        ),
        Ingredient(
          0.25,
          'cup',
          'chopped tomatoes',
        ),
      ],
    ),
    Recipe(
      label: 'Hawaiian Pizza',
      imageUrl: 'assets/15452035777_294cefced5_c.jpg',
      servings: 4,
      ingredients: [
        Ingredient(
          1,
          'item',
          'pizza',
        ),
        Ingredient(
          1,
          'cup',
          'pineapple',
        ),
        Ingredient(
          8,
          'oz',
          'ham',
        ),
      ],
    )
  ];
}
